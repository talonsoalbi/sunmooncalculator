/**
 * A planetary ephemeris calculator to complete the Sun/Moon calculator, also without 
 * using JPARSEC library or any other dependency.
 * @author T. Alonso Albi - OAN (Spain), email t.alonso@oan.es
 * @version July 1, 2020 (method calcEarth removed (-getSun used), precession in ecliptic system)
 * @version June 10, 2020 (only basic test done)
 */
public class PlanetCalculator extends SunMoonCalculator {

	/**
	 * Main constructor for planetary calculations. Time should be given in
	 * Universal Time (UT), observer angles in radians.
	 * @param year The year.
	 * @param month The month.
	 * @param day The day.
	 * @param h The hour.
	 * @param m Minute.
	 * @param s Second.
	 * @param obsLon Longitude for the observer.
	 * @param obsLat Latitude for the observer.
	 * @param obsAlt Altitude of the observer in m.
	 */
	public PlanetCalculator(int year, int month, int day, int h, int m, int s, 
			double obsLon, double obsLat, int obsAlt) {
		super(year, month, day, h, m, s, obsLon, obsLat, obsAlt);
	}

	/* Planetary inverse masses */
	private static double amas[] = { 
			6023600.0,       /* Mercury */			408523.5,       /* Venus   */
			328900.5,       /* EMB     */			3098710.0,       /* Mars    */
			1047.355,     /* Jupiter */			3498.5,       /* Saturn  */
			22869.0,       /* Uranus  */			19314.0      /* Neptune */ };

	/*
	 * Tables giving the mean Keplerian elements, limited to t^2 terms:
	 *   a       semi-major axis (au) 			dlm     mean longitude (deg and arcsec)
	 *   e       eccentricity 					pi      longitude of the perihelion (deg and arcsec)
	 *   dinc    inclination (deg and arcsec) 	omega   longitude of the ascending node (deg and arcsec)
	 */
	private static double a[][] = {
	       new double[] {  0.3870983098,           0.0,     0.0 },  /* Mercury */
	       new double[] {  0.7233298200,           0.0,     0.0 },  /* Venus   */
	       new double[] {  1.0000010178,           0.0,     0.0 },  /* EMB     */
	       new double[] {  1.5236793419,         3e-10,     0.0 },  /* Mars    */
	       new double[] {  5.2026032092,     19132e-10, -39e-10 },  /* Jupiter */
	       new double[] {  9.5549091915, -0.0000213896, 444e-10 },  /* Saturn  */
	       new double[] { 19.2184460618,     -3716e-10, 979e-10 },  /* Uranus  */
	       new double[] { 30.1103868694,    -16635e-10, 686e-10 }   /* Neptune */
	};

	private static double dlm[][] = {
			new double[] { 252.25090552, 5381016286.88982,  -1.92789 },
			new double[] { 181.97980085, 2106641364.33548,   0.59381 },
			new double[] { 100.46645683, 1295977422.83429,  -2.04411 },
			new double[] { 355.43299958,  689050774.93988,   0.94264 },
			new double[] {  34.35151874,  109256603.77991, -30.60378 },
			new double[] {  50.07744430,   43996098.55732,  75.61614 },
			new double[] { 314.05500511,   15424811.93933,  -1.75083 },
			new double[] { 304.34866548,    7865503.20744,   0.21103 }
	};

	private static double e[][] = {
			new double[] { 0.2056317526,  0.0002040653,    -28349e-10 },
			new double[] { 0.0067719164, -0.0004776521,     98127e-10 },
			new double[] { 0.0167086342, -0.0004203654, -0.0000126734 },
			new double[] { 0.0934006477,  0.0009048438,    -80641e-10 },
			new double[] { 0.0484979255,  0.0016322542, -0.0000471366 },
			new double[] { 0.0555481426, -0.0034664062, -0.0000643639 },
			new double[] { 0.0463812221, -0.0002729293,  0.0000078913 },
			new double[] { 0.0094557470,  0.0000603263,           0.0 }
	};

	private static double pi[][] = {
			new double[] {  77.45611904,  5719.11590,   -4.83016 },
			new double[] { 131.56370300,   175.48640, -498.48184 },
			new double[] { 102.93734808, 11612.35290,   53.27577 },
			new double[] { 336.06023395, 15980.45908,  -62.32800 },
			new double[] {  14.33120687,  7758.75163,  259.95938 },
			new double[] {  93.05723748, 20395.49439,  190.25952 },
			new double[] { 173.00529106,  3215.56238,  -34.09288 },
			new double[] {  48.12027554,  1050.71912,   27.39717 }
	};

	private static double dinc[][] = {
			new double[] { 7.00498625, -214.25629,   0.28977 },
			new double[] { 3.39466189,  -30.84437, -11.67836 },
			new double[] {        0.0,  469.97289,  -3.35053 },
			new double[] { 1.84972648, -293.31722,  -8.11830 },
			new double[] { 1.30326698,  -71.55890,  11.95297 },
			new double[] { 2.48887878,   91.85195, -17.66225 },
			new double[] { 0.77319689,  -60.72723,   1.25759 },
			new double[] { 1.76995259,    8.12333,   0.08135 }
	};

	private static double omega[][] = {
			new double[] {  48.33089304,  -4515.21727,  -31.79892 },
			new double[] {  76.67992019, -10008.48154,  -51.32614 },
			new double[] { 174.87317577,  -8679.27034,   15.34191 },
			new double[] {  49.55809321, -10620.90088, -230.57416 },
			new double[] { 100.46440702,   6362.03561,  326.52178 },
			new double[] { 113.66550252,  -9240.19942,  -66.23743 },
			new double[] {  74.00595701,   2669.15033,  145.93964 },
			new double[] { 131.78405702,   -221.94322,   -0.78728 }
	};

	/* Tables for trigonometric terms to be added to the mean elements of */
	/* the semi-major axes */
	private static double kp[][] = {
			new double[] {   69613, 75645, 88306, 59899, 15746, 71087, 142173,  3086,    0 },
			new double[] {   21863, 32794, 26934, 10931, 26250, 43725,  53867, 28939,    0 },
			new double[] {   16002, 21863, 32004, 10931, 14529, 16368,  15318, 32794,    0 },
			new double[] {    6345,  7818, 15636,  7077,  8184, 14163,   1107,  4872,    0 },
			new double[] {    1760,  1454,  1167,   880,   287,  2640,     19,  2047, 1454 },
			new double[] {     574,     0,   880,   287,    19,  1760,   1167,   306,  574 },
			new double[] {     204,     0,   177,  1265,     4,   385,    200,   208,  204 },
			new double[] {       0,   102,   106,     4,    98,  1367,    487,   204,    0 }
	};

	private static double ca[][] = {
			new double[] {       4,    -13,    11,   -9,    -9,   -3,     -1,     4,     0 },
			new double[] {    -156,     59,   -42,    6,    19,  -20,    -10,   -12,     0 },
			new double[] {      64,   -152,    62,   -8,    32,  -41,     19,   -11,     0 },
			new double[] {     124,    621,  -145,  208,    54,  -57,     30,    15,     0 },
			new double[] {  -23437,  -2634,  6601, 6259, -1507,-1821,   2620, -2115, -1489 },
			new double[] {   62911,-119919, 79336,17814,-24241,12068,   8306, -4893,  8902 },
			new double[] {  389061,-262125,-44088, 8387,-22976,-2093,   -615, -9720,  6633 },
			new double[] { -412235,-157046,-31430,37817, -9740,  -13,  -7449,  9644,     0 }
	};

	private static double sa[][] = {
			new double[] {     -29,    -1,     9,     6,    -6,     5,     4,     0,     0 },
			new double[] {     -48,  -125,   -26,   -37,    18,   -13,   -20,    -2,     0 },
			new double[] {    -150,   -46,    68,    54,    14,    24,   -28,    22,     0 },
			new double[] {    -621,   532,  -694,   -20,   192,   -94,    71,   -73,     0 },
			new double[] {  -14614,-19828, -5869,  1881, -4372, -2255,   782,   930,   913 },
			new double[] {  139737,     0, 24667, 51123, -5102,  7429, -4095, -1976, -9566 },
			new double[] { -138081,     0, 37205,-49039,-41901,-33872,-27037,-12474, 18797 },
			new double[] {       0, 28492,133236, 69654, 52322,-49577,-26430, -3593,     0 }
	};

	/* Tables giving the trigonometric terms to be added to the mean */
	/* elements of the mean longitudes */
	private static double kq[][] = {
			new double[] {   3086,15746,69613,59899,75645,88306, 12661,  2658,    0,     0 },
			new double[] {  21863,32794,10931,   73, 4387,26934,  1473,  2157,    0,     0 },
			new double[] {     10,16002,21863,10931, 1473,32004,  4387,    73,    0,     0 },
			new double[] {     10, 6345, 7818, 1107,15636, 7077,  8184,   532,   10,     0 },
			new double[] {     19, 1760, 1454,  287, 1167,  880,   574,  2640,   19,  1454 },
			new double[] {     19,  574,  287,  306, 1760,   12,    31,    38,   19,   574 },
			new double[] {      4,  204,  177,    8,   31,  200,  1265,   102,    4,   204 },
			new double[] {      4,  102,  106,    8,   98, 1367,   487,   204,    4,   102 }
	};

	private static double cl[][] = {
			new double[] {      21,   -95, -157,   41,   -5,   42,  23,  30,      0,     0 },
			new double[] {    -160,  -313, -235,   60,  -74,  -76, -27,  34,      0,     0 },
			new double[] {    -325,  -322,  -79,  232,  -52,   97,  55, -41,      0,     0 },
			new double[] {    2268,  -979,  802,  602, -668,  -33, 345, 201,    -55,     0 },
			new double[] {    7610, -4997,-7689,-5841,-2617, 1115,-748,-607,   6074,   354 },
			new double[] {  -18549, 30125,20012, -730,  824,   23,1289,-352, -14767, -2062 },
			new double[] { -135245,-14594, 4197,-4030,-5630,-2898,2540,-306,   2939,  1986 },
			new double[] {   89948,  2103, 8963, 2695, 3682, 1648, 866,-154,  -1963,  -283 }
	};

	private static double sl[][] = {
			new double[] {   -342,   136,  -23,   62,   66,  -52, -33,    17,     0,     0 },
			new double[] {    524,  -149,  -35,  117,  151,  122, -71,   -62,     0,     0 },
			new double[] {   -105,  -137,  258,   35, -116,  -88,-112,   -80,     0,     0 },
			new double[] {    854,  -205, -936, -240,  140, -341, -97,  -232,   536,     0 },
			new double[] { -56980,  8016, 1012, 1448,-3024,-3710, 318,   503,  3767,   577 },
			new double[] { 138606,-13478,-4964, 1441,-1319,-1482, 427,  1236, -9167, -1918 },
			new double[] {  71234,-41116, 5334,-4935,-1848,   66, 434, -1748,  3780,  -701 },
			new double[] { -47645, 11647, 2166, 3194,  679,    0,-244,  -419, -2531,    48 }
	};
	
	/**
	 * Computes the position of a given planet.
	 * @param body The body to compute.
	 * @return Ecliptic longitude, latitude (radians, of date), distance (AU), and apparent radius (radians), as
	 * given also in other methods for the Sun and the Moon.
	 */
	private double[] getPlanet(BODY body) {		
		if (body == BODY.Moon) return super.getMoon();
		if (body == BODY.Sun) return super.getSun(); // Not required, but faster

		// Compute Earth position in the Solar System (= -geocentric position of sun)
		double sunNow[] = getSun();
		double eX1 = -sunNow[2] * Math.cos(sunNow[0]) * Math.cos(sunNow[1]);
		double eY1 = -sunNow[2] * Math.sin(sunNow[0]) * Math.cos(sunNow[1]);
		double eZ1 = -sunNow[2] * Math.sin(sunNow[1]);
		double earth[] = new double[] {eX1, eY1, eZ1, 0, 0, 0}; // Using vel = 0 since aberration is already corrected in getSun()

		// Compute position and velocity of target in the SS
		double target[] = calcBody(body);

		// Compute geocentric geometric position
		double px = target[0] - earth[0];
		double py = target[1] - earth[1];
		double pz = target[2] - earth[2];

		double vx = target[3] - earth[3];
		double vy = target[4] - earth[4];
		double vz = target[5] - earth[5];

		// First order light-time correction
		double dist = Math.sqrt(px * px + py * py + pz * pz) * LIGHT_TIME_DAYS_PER_AU;
		px = px - vx * dist;
		py = py - vy * dist;
		pz = pz - vz * dist;
		
		// Output geocentric apparent ecliptic position of date
		double radius = body.eqRadius / AU;
		double distance = Math.sqrt(px * px + py * py + pz * pz);
		double longitude = Math.atan2(py, px);
		double latitude = Math.atan2(pz / Math.hypot(px, py), 1.0);
		return new double[] {longitude, latitude, distance, Math.atan(radius / distance)};
	}
	
	/** 
	 * Computes the position and velocity of a given body. Based on iauPlan94 program as published 
	 * by SOFA (Standards Of Fundamental Astronomy) software collection. Original source: Simon et al.
	 * Fortran code from Simon, J.L., Bretagnon, P., Chapront, J., Chapront-Touze, M., Francou, G., and 
	 * Laskar, J., Astronomy and Astrophysics 282, 663 (1994).
	 *  
	 * Expected maximum error over the interval 1800-2100 A.D.: 
	 * better than 110" for Jupiter, Saturn, and Uranus. 40" for Mars, <=20" for the others.
	 * Over 1000-3000 A.D: about 1.5 times the previous errors. Note 150" means around 10s of error in rise/set times, 
	 * so the expected error is below 10s for all bodies even in this wide interval.
	 * 
	 * @param body The body.
	 * @return Position and velocity, ecliptic of date.
	 */
	private double[] calcBody(BODY body) {
		if (body == BODY.Sun) return new double[] {0, 0, 0, 0, 0, 0}; // For the Sun, located at barycenter
		int np = body.index;
		
		/* Gaussian constant */
		double GK = 0.017202098950;

		/* Maximum number of iterations allowed to solve Kepler's equation */
		int KMAX = 10;
	
		/* Time: Julian millennia since J2000.0 */
		double t = super.t / 10.0;

		/* Compute the mean elements */
		double da = a[np][0] + (a[np][1] + a[np][2] * t) * t;
		double dl = (3600.0 * dlm[np][0] + (dlm[np][1] + dlm[np][2] * t) * t) * ARCSEC_TO_RAD;
		double de = e[np][0] + (e[np][1] + e[np][2] * t) * t;
		double dp = normalizeRadians((3600.0 * pi[np][0] + (pi[np][1] + pi[np][2] * t) * t) * ARCSEC_TO_RAD);
		double di = (3600.0 * dinc[np][0] + (dinc[np][1] + dinc[np][2] * t) * t) * ARCSEC_TO_RAD;
		double dom = normalizeRadians((3600.0 * omega[np][0] + (omega[np][1] + omega[np][2] * t) * t) * ARCSEC_TO_RAD);

		/* Apply the trigonometric terms */
		double dmu = 0.35953620 * t, arga = 0, argl = 0;
		for (int k = 0; k < 8; k++) {
			arga = kp[np][k] * dmu;
			argl = kq[np][k] * dmu;
			da += (ca[np][k] * Math.cos(arga) + sa[np][k] * Math.sin(arga)) * 1e-7;
			dl += (cl[np][k] * Math.cos(argl) + sl[np][k] * Math.sin(argl)) * 1e-7;
		}
		arga = kp[np][8] * dmu;
		da += t * (ca[np][8] * Math.cos(arga) + sa[np][8] * Math.sin(arga)) * 1e-7;
		for (int k = 8; k < 10; k++) {
			argl = kq[np][k] * dmu;
			dl += t * (cl[np][k] * Math.cos(argl) + sl[np][k] * Math.sin(argl)) * 1e-7;
		}
		dl = normalizeRadians(dl);

		/* Iterative soln. of Kepler's equation to get eccentric anomaly */
		double am = dl - dp;
		double ae = am + de * Math.sin(am);
		int k = 0;
		double dae = 1.0;
		while (k < KMAX && Math.abs(dae) > 1e-12) {
			dae = (am - ae + de * Math.sin(ae)) / (1.0 - de * Math.cos(ae));
			ae += dae;
			k++;
		}

		/* True anomaly */
		double ae2 = ae / 2.0;
		double at = 2.0 * Math.atan2(Math.sqrt((1.0 + de) / (1.0 - de)) * Math.sin(ae2), Math.cos(ae2));

		/* Distance (au) and speed (radians per day) */
		double r = da * (1.0 - de * Math.cos(ae));
		double v = GK * Math.sqrt((1.0 + 1.0 / amas[np]) / (da * da * da));

		double si2 = Math.sin(di / 2.0);
		double xq = si2 * Math.cos(dom);
		double xp = si2 * Math.sin(dom);
		double tl = at + dp;
		double xsw = Math.sin(tl);
		double xcw = Math.cos(tl);
		double xm2 = 2.0 * (xp * xcw - xq * xsw);
		double xf = da / Math.sqrt(1 - de * de);
		double ci2 = Math.cos(di / 2.0);
		double xms = (de * Math.sin(dp) + xsw) * xf;
		double xmc = (de * Math.cos(dp) + xcw) * xf;
		double xpxq2 = 2 * xp * xq;

		/* Position (J2000.0 ecliptic x,y,z in au) */
		double x = r * (xcw - xm2 * xp);
		double y = r * (xsw + xm2 * xq);
		double z = r * (-xm2 * ci2);
		double pos[] = new double[] {x, y, z};

		/* Velocity (J2000.0 ecliptic xdot,ydot,zdot in au/d) */
		x = v * (( -1.0 + 2.0 * xp * xp) * xms + xpxq2 * xmc);
		y = v * ((  1.0 - 2.0 * xq * xq) * xmc - xpxq2 * xms);
		z = v * (2.0 * ci2 * (xp * xms + xq * xmc));
		double vel[] = new double[] {x, y, z};

		// Additional computations needed outside the referenced program
		// Precession directly to ecliptic of date in ecliptic system, without rotation to equatorial
		pos = precessionFromJ2000(pos);
		vel = precessionFromJ2000(vel);
		
		return new double[] {pos[0], pos[1], pos[2], vel[0], vel[1], vel[2]};
	}
	
	/**
	 * Precess coordinates from J2000 (t0 = 0) to a given date using the classical method 
	 * by Lieske 1979, but in the ecliptic system.
	 * @param p The x, y, z ecliptic positions.
	 * @return Output x, y, z ecliptic positions for the final date.
	 */
	private double[] precessionFromJ2000(double p[]) {
		double t0 = 0, dt = t - t0;
		double lon = 174.876383888889 + (((3289.4789 + .60622 * t0) * t0) + ((-869.8089 - .50491 * t0) + .03536 * dt) * dt) / 3600;
		double ang = ((47.0029 - (0.06603 - .000598 * t0) * t0) + ((-.03302 + .000598 * t0) + .00006 * dt) * dt) * dt / 3600;
		double prec = ((5029.0966 + (2.22226 - .000042 * t0) * t0) + ((1.11113 - .000042 * t0) - .000006 * dt) * dt) * dt / 3600;
		lon *= DEG_TO_RAD;
		ang *= DEG_TO_RAD;
		prec *= DEG_TO_RAD;
		double c1 = Math.cos(lon + prec), c2 = Math.cos(ang), c3 = Math.cos(lon);
		double s1 = Math.sin(lon + prec), s2 = Math.sin(ang), s3 = Math.sin(lon);

		double mat1[] = new double[] {c1 * c3 + s1 * c2 * s3, c1 * s3 - s1 * c2 * c3, -s1 * s2};
		double mat2[] = new double[] {s1 * c3 - c1 * c2 * s3, s1 * s3 + c1 * c2 * c3, c1 * s2};
		double mat3[] = new double[] {s2 * s3, -s2 * c3, c2};
		
		double x[] = new double[] {
			p[0] * mat1[0] + p[1] * mat1[1] + p[2] * mat1[2],
			p[0] * mat2[0] + p[1] * mat2[1] + p[2] * mat2[2],
			p[0] * mat3[0] + p[1] * mat3[1] + p[2] * mat3[2]
		};
		return x;
	}
	
	/**
	 * Computes an accurate rise/set/transit time for a moving object.
	 * @param riseSetJD Start date for the event.
	 * @param index Event identifier.
	 * @param niter Maximum number of iterations.
	 * @param body The body.
	 * @return The Julian day in UT for the event, 1s accuracy.
	 */
	private double obtainAccurateRiseSetTransit(double riseSetJD, EVENT index, int niter, BODY body) {
		double step = -1;
		for (int i = 0; i< niter; i++) {
			if (riseSetJD == -1) return riseSetJD; // -1 means no rise/set from that location
			setUTDate(riseSetJD);
			Ephemeris out = doCalc(getPlanet(body));
			
			double val = out.rise;
			if (index == EVENT.SET) val = out.set;
			if (index == EVENT.TRANSIT) val = out.transit;
			step = Math.abs(riseSetJD - val);
			riseSetJD = val;
			if (step <= 1.0 / SECONDS_PER_DAY) break; // convergency reached
		}
		if (step > 1.0 / SECONDS_PER_DAY) return -1; // did not converge => without rise/set/transit in this date
		return riseSetJD;
	}
	
	/** 
	 * Calculates ephemeris for a planet.
	 * @param body The target body.
	 * @return The ephemeris for the body.
	 */
	public Ephemeris calcPlanet(BODY body) {
		double jd = jd_UT;
		
		// First the Sun (needed for illumination phase)
		sun = doCalc(getSun());

		Ephemeris planet = doCalc(getPlanet(body));

		int niter = 15; // Number of iterations to get accurate rise/set/transit times
		planet.rise = obtainAccurateRiseSetTransit(planet.rise, EVENT.RISE, niter, body);
		planet.set = obtainAccurateRiseSetTransit(planet.set, EVENT.SET, niter, body);
		planet.transit = obtainAccurateRiseSetTransit(planet.transit, EVENT.TRANSIT, niter, body);
		if (planet.transit == -1) {
			planet.transitElevation = 0;
		} else {
			// Update maximum elevation
			setUTDate(planet.transit);
			planet.transitElevation = doCalc(getPlanet(body)).transitElevation;
		}
		
		// Compute illumination phase percentage for the planet
		if (body != BODY.Sun) getIlluminationPhase(planet);
		
		setUTDate(jd);
		return planet;
	}
	
	/**
	 * Main test program.
	 * @param args Not used.
	 */
	public static void main(String args[]) {
		System.out.println("PlanetCalculator test run");
		
		try {
			int year = 2020, month = 6, day = 29, h = 11, m = 48, s = 58; // UT
			double obsLon = -(3 + (37 + 52 / 60.0) / 60.0) * DEG_TO_RAD, obsLat = (40 + (32 + 32 / 60.0) / 60.0) * DEG_TO_RAD;
			int obsAlt = 0;
			BODY body = BODY.MARS;
			
			PlanetCalculator pc = new PlanetCalculator(year, month, day, h, m, s, obsLon, obsLat, obsAlt);
			Ephemeris planet = pc.calcPlanet(body);
			
			String degSymbol = "\u00b0";
			System.out.println(body.name());
			System.out.println(" Az:       "+(float) (planet.azimuth * RAD_TO_DEG)+degSymbol);
			System.out.println(" El:       "+(float) (planet.elevation * RAD_TO_DEG)+degSymbol);
			System.out.println(" Dist:     "+(float) (planet.distance)+" AU");
			System.out.println(" RA:       "+(float) (planet.rightAscension * RAD_TO_DEG)+degSymbol);
			System.out.println(" DEC:      "+(float) (planet.declination * RAD_TO_DEG)+degSymbol);
			System.out.println(" Ill:      "+(float) (planet.illuminationPhase)+"%");
			System.out.println(" ang.R:    "+(float) (planet.angularRadius * RAD_TO_DEG * 3600)+"\"");
			System.out.println(" Rise:     "+SunMoonCalculator.getDateAsString(planet.rise));
			System.out.println(" Set:      "+SunMoonCalculator.getDateAsString(planet.set));
			System.out.println(" Transit:  "+SunMoonCalculator.getDateAsString(planet.transit)+" (elev. "+(float) (planet.transitElevation * RAD_TO_DEG)+degSymbol+")");

			// Can be used in the year interval 1000-3000, but 1800-2100 is recommended
			// Expected accuracy (maximum errors) 1800-2100:
			// Jupiter, Saturn, and Uranus: 2 arcminutes, 8s in rise/set/transit times
			// Mars: 0.7 arcminutes, 3s in rise/set/transit times
			// Other planets: <0.5 arcminutes, 1s in rise/set/transit times
			// Sun/Moon: same results
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
