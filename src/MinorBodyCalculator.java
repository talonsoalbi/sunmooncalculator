/**
 * A minor body ephemeris calculator to complete the Sun/Moon calculator, also without 
 * using JPARSEC library or any other dependency.
 * @author T. Alonso Albi - OAN (Spain), email t.alonso@oan.es
 * @version February 19, 2021
 */
public class MinorBodyCalculator extends SunMoonCalculator {

	static class OrbitalElement {
		/** Body name. */
		public String name;
		/** Semimajor axis, AU. */
		public double semimajorAxis;
		/** Eccentricity. */
		public double eccentricity;
		/** Mean longitude, radians. */
		public double meanLongitude;
		/** Ascending node longitude, radians. */
		public double ascendingNodeLongitude;
		/** Inclination, radians. */
		public double inclination;
		/** Perihelion longitude, radians. */
		public double perihelionLongitude;
		/** Julian day of reference for the elements. */
		public double epoch;
		/** Equinox of the elements, assumed to be J2000. */
		public double equinox = J2000;
		
		/**
		 * Constructor for a set of orbital elements.
		 * @param n Body name.
		 * @param a Semimajor axis.
		 * @param e Eccentricity.
		 * @param dlm Mean longitude at epoch.
		 * @param omega Ascending node longitude.
		 * @param dinc Inclination.
		 * @param pi Perihelion longitude.
		 * @param ep Epoch of elements.
		 */
		public OrbitalElement (String n, double a, double e, double dlm, 
				double omega, double dinc, double pi, double ep) {
			name = n;
			semimajorAxis = a;
			eccentricity = e;
			meanLongitude = dlm;
			inclination = dinc;
			ascendingNodeLongitude = omega;
			perihelionLongitude = pi;
			epoch = ep;
		}
	}
	
	/** Orbital elements of the minor body. */
	private OrbitalElement orbit;
	
	/**
	 * Main constructor for minor body calculations. Time should be given in
	 * Universal Time (UT), observer angles in radians.
	 * @param orbit The orbital elements of the minor body.
	 * @param year The year.
	 * @param month The month.
	 * @param day The day.
	 * @param h The hour.
	 * @param m Minute.
	 * @param s Second.
	 * @param obsLon Longitude for the observer.
	 * @param obsLat Latitude for the observer.
	 * @param obsAlt Altitude of the observer in m.
	 */
	public MinorBodyCalculator(OrbitalElement orbit, int year, int month, int day, int h, int m, int s, 
			double obsLon, double obsLat, int obsAlt) {
		super(year, month, day, h, m, s, obsLon, obsLat, obsAlt);
		this.orbit = orbit;
	}

	/**
	 * Computes the position of a given minor body.
	 * @return Ecliptic longitude, latitude (radians, of date), distance (AU), and apparent radius (radians), as
	 * given also in other methods for the Sun and the Moon.
	 */
	private double[] getMinorBody() {		
		// Compute Earth position in the Solar System (= -geocentric position of sun)
		double sunNow[] = getSun();
		double eX1 = -sunNow[2] * Math.cos(sunNow[0]) * Math.cos(sunNow[1]);
		double eY1 = -sunNow[2] * Math.sin(sunNow[0]) * Math.cos(sunNow[1]);
		double eZ1 = -sunNow[2] * Math.sin(sunNow[1]);
		double earth[] = new double[] {eX1, eY1, eZ1, 0, 0, 0}; // Using vel = 0 since aberration is already corrected in getSun()

		// Compute position and velocity of target in the SS
		double target[] = calcBody();

		// Compute geocentric geometric position
		double px = target[0] - earth[0];
		double py = target[1] - earth[1];
		double pz = target[2] - earth[2];

		double vx = target[3] - earth[3];
		double vy = target[4] - earth[4];
		double vz = target[5] - earth[5];

		// First order light-time correction
		double dist = Math.sqrt(px * px + py * py + pz * pz) * LIGHT_TIME_DAYS_PER_AU;
		px = px - vx * dist;
		py = py - vy * dist;
		pz = pz - vz * dist;
		
		// Output geocentric apparent ecliptic position of date
		double radius = 0 / AU;
		double distance = Math.sqrt(px * px + py * py + pz * pz);
		double longitude = Math.atan2(py, px);
		double latitude = Math.atan2(pz / Math.hypot(px, py), 1.0);
		return new double[] {longitude, latitude, distance, Math.atan(radius / distance)};
	}
	
	/** 
	 * Computes the position and velocity of a given body. Based on iauPlan94 program as published 
	 * by SOFA (Standards Of Fundamental Astronomy) software collection. Original source: Simon et al.
	 * Fortran code from Simon, J.L., Bretagnon, P., Chapront, J., Chapront-Touze, M., Francou, G., and 
	 * Laskar, J., Astronomy and Astrophysics 282, 663 (1994).
	 * 
	 * @return Position and velocity, ecliptic of date.
	 */
	private double[] calcBody() {
		/* Gaussian constant */
		double GK = 0.017202098950;

		/* Maximum number of iterations allowed to solve Kepler's equation */
		int KMAX = 10;
	
		/* Compute the mean elements */
		double da = orbit.semimajorAxis;
		double dl = orbit.meanLongitude;
		double de = orbit.eccentricity;
		double dp = orbit.perihelionLongitude;
		double di = orbit.inclination;
		double dom = orbit.ascendingNodeLongitude;

		/* Speed (radians per day) */
		double v = GK * Math.sqrt(1.0 / (da * da * da));
		/* Iterative soln. of Kepler's equation to get eccentric anomaly */
		double am = normalizeRadians(dl - dp + v * (jd_UT + TTminusUT / SECONDS_PER_DAY - orbit.epoch));
		double ae = am + de * Math.sin(am);
		int k = 0;
		double dae = 1.0;
		while (k < KMAX && Math.abs(dae) > 1e-12) {
			dae = (am - ae + de * Math.sin(ae)) / (1.0 - de * Math.cos(ae));
			ae += dae;
			k++;
		}

		/* True anomaly */
		double ae2 = ae / 2.0;
		double at = 2.0 * Math.atan2(Math.sqrt((1.0 + de) / (1.0 - de)) * Math.sin(ae2), Math.cos(ae2));

		/* Distance (au) */
		double r = da * (1.0 - de * Math.cos(ae));

		double si2 = Math.sin(di / 2.0);
		double xq = si2 * Math.cos(dom);
		double xp = si2 * Math.sin(dom);
		double tl = at + dp;
		double xsw = Math.sin(tl);
		double xcw = Math.cos(tl);
		double xm2 = 2.0 * (xp * xcw - xq * xsw);
		double xf = da / Math.sqrt(1 - de * de);
		double ci2 = Math.cos(di / 2.0);
		double xms = (de * Math.sin(dp) + xsw) * xf;
		double xmc = (de * Math.cos(dp) + xcw) * xf;
		double xpxq2 = 2 * xp * xq;

		/* Position (J2000.0 ecliptic x,y,z in au) */
		double x = r * (xcw - xm2 * xp);
		double y = r * (xsw + xm2 * xq);
		double z = r * (-xm2 * ci2);
		double pos[] = new double[] {x, y, z};

		/* Velocity (J2000.0 ecliptic xdot,ydot,zdot in au/d) */
		x = v * (( -1.0 + 2.0 * xp * xp) * xms + xpxq2 * xmc);
		y = v * ((  1.0 - 2.0 * xq * xq) * xmc - xpxq2 * xms);
		z = v * (2.0 * ci2 * (xp * xms + xq * xmc));
		double vel[] = new double[] {x, y, z};

		// Additional computations needed outside the referenced program
		// Precession directly to ecliptic of date in ecliptic system, without rotation to equatorial
		pos = precession(pos);
		vel = precession(vel);
		
		return new double[] {pos[0], pos[1], pos[2], vel[0], vel[1], vel[2]};
	}
	
	/**
	 * Precess coordinates from the equinox of the orbital elements to a given date using the classical method 
	 * by Lieske 1979, but in the ecliptic system.
	 * @param p The x, y, z ecliptic positions.
	 * @return Output x, y, z ecliptic positions for the final date.
	 */
	private double[] precession(double p[]) {
		double t0 = (orbit.equinox - J2000) / JULIAN_DAYS_PER_CENTURY, dt = t - t0;
		double lon = 174.876383888889 + (((3289.4789 + .60622 * t0) * t0) + ((-869.8089 - .50491 * t0) + .03536 * dt) * dt) / 3600;
		double ang = ((47.0029 - (0.06603 - .000598 * t0) * t0) + ((-.03302 + .000598 * t0) + .00006 * dt) * dt) * dt / 3600;
		double prec = ((5029.0966 + (2.22226 - .000042 * t0) * t0) + ((1.11113 - .000042 * t0) - .000006 * dt) * dt) * dt / 3600;
		lon *= DEG_TO_RAD;
		ang *= DEG_TO_RAD;
		prec *= DEG_TO_RAD;
		double c1 = Math.cos(lon + prec), c2 = Math.cos(ang), c3 = Math.cos(lon);
		double s1 = Math.sin(lon + prec), s2 = Math.sin(ang), s3 = Math.sin(lon);

		double mat1[] = new double[] {c1 * c3 + s1 * c2 * s3, c1 * s3 - s1 * c2 * c3, -s1 * s2};
		double mat2[] = new double[] {s1 * c3 - c1 * c2 * s3, s1 * s3 + c1 * c2 * c3, c1 * s2};
		double mat3[] = new double[] {s2 * s3, -s2 * c3, c2};
		
		double x[] = new double[] {
			p[0] * mat1[0] + p[1] * mat1[1] + p[2] * mat1[2],
			p[0] * mat2[0] + p[1] * mat2[1] + p[2] * mat2[2],
			p[0] * mat3[0] + p[1] * mat3[1] + p[2] * mat3[2]
		};
		return x;
	}
	
	/**
	 * Computes an accurate rise/set/transit time for a moving object.
	 * @param riseSetJD Start date for the event.
	 * @param index Event identifier.
	 * @param niter Maximum number of iterations.
	 * @return The Julian day in UT for the event, 1s accuracy.
	 */
	private double obtainAccurateRiseSetTransit(double riseSetJD, EVENT index, int niter) {
		double step = -1;
		for (int i = 0; i< niter; i++) {
			if (riseSetJD == -1) return riseSetJD; // -1 means no rise/set from that location
			setUTDate(riseSetJD);
			Ephemeris out = doCalc(getMinorBody());
			
			double val = out.rise;
			if (index == EVENT.SET) val = out.set;
			if (index == EVENT.TRANSIT) val = out.transit;
			step = Math.abs(riseSetJD - val);
			riseSetJD = val;
			if (step <= 1.0 / SECONDS_PER_DAY) break; // convergency reached
		}
		if (step > 1.0 / SECONDS_PER_DAY) return -1; // did not converge => without rise/set/transit in this date
		return riseSetJD;
	}
	
	/** 
	 * Calculates ephemeris for a minor body.
	 * @return The ephemeris for the body.
	 */
	public Ephemeris calcMinorBody() {
		double jd = jd_UT;
		
		// First the Sun (needed for illumination phase)
		sun = doCalc(getSun());

		Ephemeris minorBody = doCalc(getMinorBody());

		int niter = 15; // Number of iterations to get accurate rise/set/transit times
		minorBody.rise = obtainAccurateRiseSetTransit(minorBody.rise, EVENT.RISE, niter);
		minorBody.set = obtainAccurateRiseSetTransit(minorBody.set, EVENT.SET, niter);
		minorBody.transit = obtainAccurateRiseSetTransit(minorBody.transit, EVENT.TRANSIT, niter);
		if (minorBody.transit == -1) {
			minorBody.transitElevation = 0;
		} else {
			// Update maximum elevation
			setUTDate(minorBody.transit);
			minorBody.transitElevation = doCalc(getMinorBody()).transitElevation;
		}
		
		// Compute illumination phase percentage for the planet
		getIlluminationPhase(minorBody);
		
		setUTDate(jd);
		return minorBody;
	}
	
	/**
	 * Main test program.
	 * @param args Not used.
	 */
	public static void main(String args[]) {
		System.out.println("MinorBodyCalculator test run");
		
		try {
			// Basic main data for the ephemerides
			int year = 2020, month = 1, day = 1, h = 0, m = 0, s = 0; // UT
			double obsLon = 0 * DEG_TO_RAD, obsLat = 40 * DEG_TO_RAD;
			int obsAlt = 0;
			
			// Construct the orbit object with the set of orbital elements for Ceres.
			// Can be any other asteroid/comet with elliptic orbit. Some important notes:
			// - I compute mean longitude as mean anomaly + perihelion longitude
			// - I compute perihelion longitude as argument of perihelion + ascending node longitude
			// This is due to the orbital elements themselves, sometimes they provide 
			// directly mean longitude and perihelion longitude, and sometimes they 
			// provide mean anomaly and argument of perihelion. For comets the elements 
			// are sometimes referred to the time of perihelion, in that case the mean 
			// anomaly is not given because is 0. 
			OrbitalElement orbit = new OrbitalElement("Ceres", 2.7670463, 0.0755347, 
					(352.23052 + (73.11528 + 80.30992)) * DEG_TO_RAD, 80.30992 * DEG_TO_RAD, 10.59351 * DEG_TO_RAD, 
					(73.11528 + 80.30992) * DEG_TO_RAD, 2458200.5);
			
			// Compute and display results
			MinorBodyCalculator mbc = new MinorBodyCalculator(orbit, year, month, day, h, m, s, obsLon, obsLat, obsAlt);
			Ephemeris minorBody = mbc.calcMinorBody();
			
			String degSymbol = "\u00b0";
			System.out.println(orbit.name);
			System.out.println(" Az:       "+(float) (minorBody.azimuth * RAD_TO_DEG)+degSymbol);
			System.out.println(" El:       "+(float) (minorBody.elevation * RAD_TO_DEG)+degSymbol);
			System.out.println(" Dist:     "+(float) (minorBody.distance)+" AU");
			System.out.println(" RA:       "+(float) (minorBody.rightAscension * RAD_TO_DEG)+degSymbol);
			System.out.println(" DEC:      "+(float) (minorBody.declination * RAD_TO_DEG)+degSymbol);
			System.out.println(" Ill:      "+(float) (minorBody.illuminationPhase)+"%");
			System.out.println(" ang.R:    "+(float) (minorBody.angularRadius * RAD_TO_DEG * 3600)+"\"");
			System.out.println(" Rise:     "+SunMoonCalculator.getDateAsString(minorBody.rise));
			System.out.println(" Set:      "+SunMoonCalculator.getDateAsString(minorBody.set));
			System.out.println(" Transit:  "+SunMoonCalculator.getDateAsString(minorBody.transit)+" (elev. "+(float) (minorBody.transitElevation * RAD_TO_DEG)+degSymbol+")");
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
